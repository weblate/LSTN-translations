# Library Space Technology Network

The LSTN project offers public library communities the chance to build and engage with space technology. The [Wolbach Library](https://library.cfa.harvard.edu/) in collaboration with the [Libre Space Foundation](https://libre.space/) is currently engaged in a pilot to build [SatNOGS](https://satnogs.org/) ground stations at five public libraries around the world. During the pilot, a metadata vocabulary and schema for small satellite missions called [Metasat](https://schema.space/), will be created and tested on the ground stations. The feedback from LSTN participants will ultimately help inform plans to expand the network and build resources to support participating library communities.  

To find out more, visit us at [https://lstn.wolba.ch/](https://lstn.wolba.ch/).

## LSTN translations

We are currently working to translate our [LSTN handbook]() into Spanish so it can be easily accessed by one of our pilot partner libraries, [Biblioteca de Santiago](https://www.bibliotecasantiago.cl/). We are crowdsourcing translations through [Weblate](https://weblate.org/en/); visit our Weblate page to contribute!  

In the future, we hope to translate the handbook into more languages, as well as translate the entire LSTN website.
